<?php

namespace App\Services;

use DateTime;

class DateDiffService
{

    public function dateDiffInString(DateTime $date): string
    {
        $now = new DateTime('now');
        $diff = ($now->getTimestamp() - $date->getTimestamp());
        if ($diff < 60) { $diff_str = "Il y a $diff seconds"; }
        elseif ($diff < 3600) { $diff = intdiv($diff, 60); $diff_str = "$diff minute(s) ago"; }
        elseif ($diff < 86400) { $diff = intdiv($diff, 3600); $diff_str = "$diff hour(s) ago"; }
        elseif ($diff < 2678400) { $diff = intdiv($diff, 86400); $diff_str = "$diff day(s) ago"; }
        else { $diff = intdiv($diff, 2678400); $diff_str = "$diff month(s) ago"; }

        return $diff_str;
    }
}
