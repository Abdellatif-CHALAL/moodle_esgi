<?php

namespace App\Services;

class UserService
{

    public function findConversationWithProfessor($user, $otherUser) {
        return $user->getStudentGroup()->getGroupProfessors()->contains($otherUser);
    }

    public function findConversationWithStudent($user, $otherUser) {
        $groups = $user->getProfessorGroups();
        foreach($groups as $group) {
            if ($group->getGroupStudents()->contains($otherUser)) {
                return true;
            }
        }
        return false;
    }

}