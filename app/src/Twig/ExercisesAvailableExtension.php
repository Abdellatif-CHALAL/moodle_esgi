<?php

namespace App\Twig;

use App\Entity\Group;
use App\Entity\Subject;
use App\Repository\GroupRepository;
use App\Repository\SubjectRepository;
use Doctrine\Common\Collections\Collection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ExercisesAvailableExtension extends AbstractExtension
{
    protected $groupRepository;
    protected $subjectRepository;

    public function __construct(SubjectRepository $subjectRepository, GroupRepository $groupRepository)
    {
        $this->subjectRepository = $subjectRepository;
        $this->groupRepository = $groupRepository;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('exercisesAvailable', [$this, 'exercisesAvailableByGroup']),
        ];
    }

    public function exercisesAvailableByGroup($void, $groupId, $user)
    {
        $exercisesAvailable = [];
        $group = $this->groupRepository->find($groupId);
        $subject = $user->getSubject();

        foreach($subject->getExercises() as $exercise) {
            if (!$group->getExercises()->contains($exercise)) {
                $exercisesAvailable[] = $exercise;
            }
        }
        return $exercisesAvailable;
    }
}
