<?php

namespace App\Twig;

use App\Repository\ExerciseRepository;
use Doctrine\Common\Collections\Collection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ClassesAvailableExtension extends AbstractExtension
{
    protected $groupRepository;
    protected $subjectRepository;

    public function __construct(ExerciseRepository $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('classesAvailable', [$this, 'classesAvailableByExercise']),
        ];
    }

    public function classesAvailableByExercise($void, $exerciseId, $user)
    {
        $groupAvailable = [];
        $exercise = $this->exerciseRepository->find($exerciseId);

        $all_groups = $user->getProfessorGroups();

        foreach($all_groups as $group) {
            if (!$exercise->getGroups()->contains($group)) {
                $groupAvailable[] = $group;
            }
        }
        return $groupAvailable;
    }
}