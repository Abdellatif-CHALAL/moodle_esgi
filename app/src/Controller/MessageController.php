<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Entity\Message;
use App\Repository\ConversationRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{

    /**
     * @Route("/message", name="message_new", methods={"POST"})
     */
    public function newMessage(Request $request, ConversationRepository $conversationRepository, EntityManagerInterface $em)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'), 405);
        }
        if(!isset($request->request)) {
            return new JsonResponse(array('status' => 'Error'), 403);
        }

        $token = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);

        if ($isValidToken) {
        
            $input = htmlspecialchars($request->request->get('messageInput'));
            $conversationId = $request->request->get('conversationId');
            $conversation = $conversationRepository->find($conversationId);

            $this->denyAccessUnlessGranted("message_create", $conversation);

            if(!is_null($input) && !empty($input)) {
                if (strlen($input) > 300) {
                    return new JsonResponse(array('status' => 'Error'), 403);
                }

                $user = $this->getUser();

                $message = new Message();
                $message->setContent($input);
                $message->setAuthor($user);
                $message->setCreatedAt(new DateTime('now'));

                $conversation->addMessage($message);
                $conversation->setLastMessage($message);

                $em->persist($message);

                $otherUser = null;
                foreach($conversation->getParticipants() as $participant) {
                    if ($participant != $this->getUser()) {
                        $otherUser = $participant;
                    }
                }

                $lastMessageRead = $conversation->getLastMessageRead();
                $lastMessageReadOtherUser = $lastMessageRead[$otherUser->getId()];

                $lastMessageRead[$this->getUser()->getId()] = $message->getId();
                $lastMessageRead[$otherUser->getId()] = $lastMessageReadOtherUser;
                $conversation->setLastMessageRead($lastMessageRead);

                $em->persist($conversation);
                $em->flush();

                $dateMessage = $message->getCreatedAt();

                return $this->json([
                    'lastMessage' => $conversation->getLastMessage()->getContent(),
                    'message_id' => $conversation->getLastMessage()->getId(),
                    'conversationId' => $conversation->getId(),
                    'message_content' => $message->getContent(),
                    'message_createdAt' => $dateMessage->format('d/m/Y H:i'),
                ], 201);
            }
        }

        return $this->json([], 403);
    }
}
