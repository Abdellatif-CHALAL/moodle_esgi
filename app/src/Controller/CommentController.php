<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\ExerciseRepository;
use App\Services\DateDiffService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment", name="comment_")
 */
class CommentController extends AbstractController
{

    /**
     * @Route("/new", name="new", methods={"POST"})
     */
    public function new(Request $request, ExerciseRepository $exerciseRepository, DateDiffService $dateDiffService)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),400);
        }
        if(!isset($request->request)) {
            return new JsonResponse(array('status' => 'Error'),400);
        }
        $input = htmlspecialchars($request->request->get('commentInput'));
        $exerciseId = $request->request->get('exerciseId');
        $exercise = $exerciseRepository->find($exerciseId);
        if (in_array("ROLE_PROFESSOR", $this->getUser()->getRoles())) {
            $this->denyAccessUnlessGranted("exerciseProfessor_new", $exercise);
        } else {
            $this->denyAccessUnlessGranted("exerciseStudent_new", $exercise->getGroups());
        }
        if(!empty($input)) {
            $comment = new Comment();
            $comment->setText($input);
            $comment->setUsers($this->getUser());
            $comment->setExercises($exercise);
            $comment->setCreatedAt(new DateTime('now'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            $lastname = $this->getUser()->getLastName();
            $firstname = $this->getUser()->getFirstName();
            return new JsonResponse([
                'commentId' => $comment->getId(),
                'comment' => $comment->getText(),
                'userlastname' => $lastname,
                'userfirstname' => $firstname,
                'createdAt' => $dateDiffService->dateDiffInString($comment->getCreatedAt()),
                'updatedAt' => $comment->getUpdatedAt()
            ], 201);
        }
    }


    /**
     * @Route("/edit", name="edit", methods={"POST"})
     */
    public function edit(Request $request, CommentRepository $commentRepository): Response
    {

        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),400);
        }
        if(!isset($request->request)) {
            return new JsonResponse(array('status' => 'Error'),400);
        }
        $input = $request->request->get('commentEditInput');
        $commentId = $request->request->get('commentId');
        if(!empty($input)) {
            $comment = $commentRepository->find($commentId);

            $this->denyAccessUnlessGranted("comment_change", $comment);

            $comment->setText($input);
            $comment->setUpdatedAt(new DateTime('now'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            $lastname = $this->getUser()->getLastName();
            $firstname = $this->getUser()->getFirstName();
            return new JsonResponse([
                'commentId' => $comment->getId(),
                'comment' => $comment->getText(),
                'userlastname' => $lastname,
                'userfirstname' => $firstname,
                'createdAt' => $comment->getCreatedAt(),
                'updatedAt' => $comment->getUpdatedAt()
            ], 200);
        }
    }

    /**
     * @Route("/delete", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, CommentRepository $commentRepository): Response
    {
        $token = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);
        $commentId = "test";
        if ($isValidToken) {
            $commentId = $request->request->get('commentId');
            $comment = $commentRepository->find($commentId);
            $this->denyAccessUnlessGranted("comment_change", $comment);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();

            return new JsonResponse([
                'commentId' => $commentId,
            ], 200);
        }

        return new JsonResponse([
            'commentId' => $commentId,
        ], 403);
    }
}
