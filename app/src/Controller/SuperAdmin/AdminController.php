<?php

namespace App\Controller\SuperAdmin;

use App\Entity\Group;
use App\Entity\User;
use App\Form\EditUserType;
use App\Form\UserType;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/super-admin")
 */
class AdminController extends AbstractController
{
    public MailerService $sendEmail;
    public UserPasswordEncoderInterface $encoder;

    public function __construct(MailerService $sendEmail, UserPasswordEncoderInterface $encoder)
    {
        $this->sendEmail = $sendEmail;
        $this->encoder = $encoder;
    }
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, GroupRepository $groupRepository): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        $admins = $userRepository->findByRoles("ROLE_ADMIN");
        return $this->render('super-admin/admin/index.html.twig', [
            'admins' => $admins,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if (!$userRepository->findOneBy(['email' => $user->getEmail()])) {

                // Create password to student
                $user->setPassword($this->generateRandomString(9, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
                $password = $user->getPassword();
                // Encode password
                $encoded = $this->encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //Add ROLE_ADMIN
                $user->setRoles(['ROLE_ADMIN']);

                $entityManager->persist($user);
                $entityManager->flush();

                // Send The credentials to student to login 
                $this->sendEmail->sendEmail($password, $user->getEmail());

                $this->addFlash('success', 'Admin created successfully');

                return $this->redirectToRoute('super_admin_index');
            }
            $this->addFlash('danger', 'The email is already exists!');
        }

        return $this->render('super-admin/admin/new.html.twig', [
            'admin' => $user,
            'form' => $form->createView(),
        ]);
    }


    public function generateRandomString($length = 6, $characters = '0123456789abcdefghijklmnopqrstuvwxyz')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        return $this->render('super-admin/admin/show.html.twig', [
            'admin' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Admin updated successfully');

            return $this->redirectToRoute('super_admin_index');
        }

        return $this->render('super-admin/admin/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $subjects = $user->getSubjects();
            $professors = $user->getProfessors();

            foreach($professors as $professor) {
                $user->removeProfessor($professor);
                $em->remove($professor);
                $em->flush();
            }

            foreach($subjects as $subject) {
                $user->removeSubject($subject);
                $em->remove($subject);
                //$em->flush();
            }
            $em->flush();

            $em->remove($user);
            $em->flush();

            $this->addFlash('success', 'Admin deleted successfully');
        }

        return $this->redirectToRoute('super_admin_index');
    }

    /**
     * @Route("/{id}", name="enable_or_disable_account", methods={"POST"})
     */
    public function enableOrDisableAdminAccount(User $user,EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());
        
        $user->setIsVerified(!$user->isVerified());
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('super_admin_index');
    }
}
