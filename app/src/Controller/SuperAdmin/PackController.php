<?php

namespace App\Controller\SuperAdmin;

use App\Entity\Pack;
use App\Form\PackType;
use App\Repository\PackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/pack",name="pack_")
 */
class PackController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(PackRepository $packRepository): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        return $this->render('super-admin/pack/index.html.twig', [
            'packs' => $packRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        $pack = new Pack();
        $form = $this->createForm(PackType::class, $pack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();


            $stripe = new \Stripe\StripeClient(
                'sk_test_51ImJIiH1ST2SneRlI5texYpM1EjkRwX5h0sXH8lWH6BxPP2sFmNCXW3KqXvOCnVFnaKxOeSZd9ZhGqaYm2D1mVyl00xvAeAezq'
            );
            $product = $stripe->products->create([
                'name' => $pack->getName(),
            ]);

            $price = $stripe->prices->create([
                'unit_amount' => $pack->getPrice()*100,
                'product' => $product->id,
                'currency' => 'eur',
                'recurring' => [
                    'interval' => 'month',
                ],
            ]);
            $pack->setStripePrice($price->id);
            $entityManager->persist($pack);
            $entityManager->flush();

            return $this->redirectToRoute('super_admin_pack_index');
        }

        return $this->render('super-admin/pack/new.html.twig', [
            'pack' => $pack,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Pack $pack): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        return $this->render('super-admin/pack/show.html.twig', [
            'pack' => $pack,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pack $pack): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());

        $form = $this->createForm(PackType::class, $pack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('super_admin_pack_index');
        }

        return $this->render('super-admin/pack/edit.html.twig', [
            'pack' => $pack,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pack $pack): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());
        
        if ($this->isCsrfTokenValid('delete'.$pack->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pack);
            $entityManager->flush();
        }

        return $this->redirectToRoute('super_admin_pack_index');
    }
}
