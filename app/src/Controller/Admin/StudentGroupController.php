<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student/group", name="student_group_")
 */
class StudentGroupController extends AbstractController
{
    /**
     * @Route("/{id}", name="index", methods={"GET"})
     */
    /*public function index(Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        //HAVE TO ADD VOTER ACCESS $GROUP BY THIS USER

        $students = $group->getGroupStudents();
        return $this->render('admin/group_student/index.html.twig', [
            'users' => $students,
            'group' => $group,
        ]);
    }*/


    /**
     * @Route("/new/{student}/{group}/", name="new")
     */
    public function new(User $student, Group $group, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit', $group);
        $this->denyAccessUnlessGranted('student_edit', $student);

        $group->addGroupStudent($student);
        $em->flush();
        return $this->redirectToRoute('admin_student_index');
    }

    /**
     * @Route("/add/{id}", name="add", methods={"POST"})
     */
    public function add(Group $group, Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit', $group);

        $added = 0;
        $admin = $this->getUser();
        foreach($admin->getStudents() as $student) {
            if (is_null($student->getStudentGroup())) {
                if (!is_null($request->get('checkbox-' . $student->getId()))) {
                    $group->addGroupStudent($student);
                    $added++;
                }
            }
        }

        $em->persist($group);
        $em->flush();

        if ($added > 0) {
            $this->addFlash('success', 'Students successfully added');
        } else {
            $this->addFlash('warning', 'No student added');
        }

        return $this->redirectToRoute('admin_group_index');
    }

    /**
     * @Route("/{group}", name="remove", methods={"POST"})
     */
    public function remove(Request $request, Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);

        if ($this->isCsrfTokenValid('deleteUsers', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $students = $this->getUser()->getStudents();
            $count = 0;
            foreach($students as $student) {
                if (!is_null($request->request->get('form-checkbox-' . $student->getId()))) {
                    $count++;
                    $group->removeGroupStudent($student);
                    $em->persist($group);
                }
            }
        
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No student has been deleted');
            } else {
                $this->addFlash('success', 'Student(s) deleted successfully from class');
            }
        }
        return $this->redirectToRoute('admin_group_show', ['id' => $group->getId()]);
    }
}
