<?php

namespace App\Controller\Admin;

use App\Entity\Subject;
use App\Form\SubjectType;
use App\Repository\SubjectRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/subject",name="subject_")
 */
class SubjectController extends AbstractController
{

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(SubjectRepository $subjectRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        return $this->render('admin/subject/index.html.twig', [
            'subjects' => $subjectRepository->findBy(["admin"=>$this->getUser()]),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, SubjectRepository $subjectRepository, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $subject = new Subject();
        $form = $this->createForm(SubjectType::class, $subject);
        $professors = $this->getUser()->getProfessors();
        $professorsAvailable = [];
        foreach($professors as $professor) {
            if (is_null($professor->getSubject())) {
                $professorsAvailable[] = $professor;
            }
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $subjects = $user->getSubjects()->toArray();
            
            foreach($subjects as $index => $s) {
                if ($s->getName() != $subject->getName()) {
                    unset($subjects[$index]);
                }
            }
            if (empty($subjects)) {
                $professorId = $request->request->get('professor');
                if (!is_null($professorId) && !empty($professorId)) {
                    $professor = $userRepository->find($professorId);
                    if (is_null($professor) || empty($professor)) {
                        $this->addFlash('danger', 'The professor has not been found');
                        return $this->render('admin/subject/new.html.twig', [
                            'subject' => $subject,
                            'professors' => $professorsAvailable,
                            'form' => $form->createView(),
                        ]);
                    }
                    if (!$user->getProfessors()->contains($professor)) {
                        $this->addFlash('danger', 'You have to select a professor of the list');
                        return $this->render('admin/subject/new.html.twig', [
                            'subject' => $subject,
                            'professors' => $professorsAvailable,
                            'form' => $form->createView(),
                        ]);
                    } else {
                        $professor->setSubject($subject);
                        $subject->setProfessor($professor);
                        $entityManager->persist($professor);
                    }
                }

                $subject->setAdmin($this->getUser());
                $entityManager->persist($subject);
                $entityManager->flush();

                $this->addFlash('success', 'Subject created successfully');

                return $this->redirectToRoute('admin_subject_index');
            }
            $this->addFlash('danger', 'The subject is already exists!');
        }

        return $this->render('admin/subject/new.html.twig', [
            'subject' => $subject,
            'professors' => $professorsAvailable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Subject $subject): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('subject_show',$subject);

        return $this->render('admin/subject/show.html.twig', [
            'subject' => $subject,
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Subject $subject): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('subject_delete',$subject);

        if ($this->isCsrfTokenValid('delete' . $subject->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subject);
            $entityManager->flush();

            $this->addFlash('danger', 'Subject deleted successfully');
        }

        return $this->redirectToRoute('admin_subject_index');
    }

    /**
     * @Route("/delete", name="delete_subjects", methods={"POST"})
     */
    public function deleteSubjects(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        if ($this->isCsrfTokenValid('deleteSubjects', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $subjects = $this->getUser()->getSubjects();
            $count = 0;
            foreach($subjects as $subject) {
                if (!is_null($request->request->get('form-checkbox-' . $subject->getId()))) {
                    $count++;
                    $em->remove($subject);
                }
            }
        
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No subject has been deleted');
            } else {
                $this->addFlash('success', 'Subject(s) deleted successfully');
            }
        }

        return $this->redirectToRoute('admin_subject_index');
    }
}
