<?php

namespace App\Controller\Admin;

use App\Entity\Pack;
use App\Entity\Transaction;
use App\Repository\PackRepository;
use App\Repository\UserRepository;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpParser\Node\Expr\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/payment", name="payment_")
 */

class PaymentController extends AbstractController
{

    public MailerService $sendEmail;

    public function __construct(MailerService $sendEmail)
    {
        $this->sendEmail = $sendEmail;
    }

    /**
     * @Route("/{pack}", name="index")
     */
    public function index(Pack $pack): Response
    {
        return $this->render('admin/payment/index.html.twig', [
            "pack"=> $pack
        ]);
    }

    /**
     * @Route("/form/{pack}", name="form")
     */
    public function form(Pack $pack): Response
    {
        if (!$this->getUser()){
            return $this->redirectToRoute('app_login');
        }
        if (!is_null($this->getUser()->getPack())){
            return $this->render('admin/payment/error.html.twig');
        }
        return $this->render('admin/payment/form.html.twig', [
            "pack"=> $pack
        ]);
    }


  /**
   * @Route("/success/{pack}", name="success")
   */
    public function success(Pack $pack, Request $request): Response
    {
        if (is_null($pack)) {
            throw new Exception("the pack has not been found", 500);
        }

        $token = $request->query->get('tok_stripe');
        $databaseToken = $this->getUser()->getStripeToken();

        $admin = $this->getUser();
        $admin->setStripeToken(null);

        if ($token != $databaseToken) {
            $this->addFlash('danger', 'An error has occured during the verification of your payment');

            return $this->render('admin/payment/index.html.twig', [
                'pack' => $pack,
            ]);
        }



        $em = $this->getDoctrine()->getManager();
        $transaction = new Transaction();
        $transaction->setDescription("Moodle Service");
        $transaction->setStartDate(new \DateTime());
        $transaction->setEndDate((new \DateTime())->modify("+1 month"));
        $transaction->setTotal($pack->getPrice());
        $transaction->setSubTotal($pack->getPrice());
        $transaction->setAdmin($this->getUser());
        $transaction->setPack($pack);
        $admin->setPack($pack);

        $em->persist($transaction);
        $em->persist($admin);
        $em->flush();
        $this->sendEmail->sendEmailPaymentPack($admin,$pack);
        $this->addFlash('success', 'Your payment has been accepted, the pack ' . $pack->getName() . ' is now activated on your account');

        return $this->redirectToRoute("account_show");
    }

  /**
   * @Route("/error", name="error",methods={"GET"})
   */
    public function error(): Response
    {
        return $this->render('admin/payment/error.html.twig', []);
    }


    /**
     * @Route("/create-checkout-session/{pack}", name="checkout",methods={"POST"})
     */
    public function checkout(Request $request,Pack $pack, EntityManagerInterface $em): Response
    {
        $stripeToken = base64_encode(random_bytes(16));

        $user = $this->getUser();
        $user->setStripeToken($stripeToken);
        $em->persist($user);
        $em->flush();

        $stripe = new \Stripe\StripeClient(
            'sk_test_51ImJIiH1ST2SneRlI5texYpM1EjkRwX5h0sXH8lWH6BxPP2sFmNCXW3KqXvOCnVFnaKxOeSZd9ZhGqaYm2D1mVyl00xvAeAezq'
        );

        $customer = $stripe->customers->create([
            'email'=> $request->request->get('email'),
            'name' =>  $request->request->get('cname'),
            'source' => $request->request->get('stripeToken')
        ]);



       $sub = $stripe->subscriptions->create([
            'customer' => $customer->id,
            'items' => [
                ['price' => $pack->getStripePrice()],
            ],
        ]);

       $user = $this->getUser();
       $user->setStripeSubscription($sub->id);
       $em->persist($user);
       $em->flush();

        return $this->redirectToRoute('payment_success',[
            'pack' =>$pack->getId(),
            'tok_stripe' => $stripeToken
        ]);

    }


    /**
     * @Route("/edit/{pack}", name="edit",methods={"GET"})
     */
    public function edit(Pack $pack,UserRepository $userRepository, PackRepository $packRepository, EntityManagerInterface $em): Response
    {

        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        $user = $userRepository->findOneBy(['email'=>$this->getUser()->getUsername()]);
        $currentNumberProfessors = $user->getProfessors()->count();
        $currentNumberStudents = $user->getStudents()->count();
        $wantedPackLimitProfessors = $pack->getNbrProfessors();
        $wantedPackLimitStudents = $pack->getNbStudents();
        if ($currentNumberProfessors > $wantedPackLimitProfessors || $currentNumberStudents > $wantedPackLimitStudents) {
            $this->addFlash('danger', 'You cannot change for this plan, you have to reduce your number of professors and students before');

            return $this->redirectToRoute('admin_transaction_index');
        }

        $user = $this->getUser();

        \Stripe\Stripe::setApiKey('sk_test_51ImJIiH1ST2SneRlI5texYpM1EjkRwX5h0sXH8lWH6BxPP2sFmNCXW3KqXvOCnVFnaKxOeSZd9ZhGqaYm2D1mVyl00xvAeAezq');
        $subscription = \Stripe\Subscription::retrieve($user->getStripeSubscription());

        $subscription = \Stripe\Subscription::update(
            $user->getStripeSubscription(),
            [
                'cancel_at_period_end' => false,
                'proration_behavior' => 'create_prorations',
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'price' => $pack->getStripePrice(),
                    ],
                ],
            ]
        );

        $user->setPack($pack);
        $em->persist($user);
        $em->flush();

        $this->sendEmail->sendEmailPaymentChangePlanPack($user,$pack);

        $this->addFlash('success', 'Your plan has been changed, and will take effect on your next monthly payment');

        return $this->redirectToRoute('admin_transaction_index');
    }
}