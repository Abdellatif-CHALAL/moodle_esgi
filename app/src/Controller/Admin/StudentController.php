<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\Subject;
use App\Entity\User;
use App\Form\EditUserType;
use App\Form\UploadType;
use App\Form\UserType;
use App\Repository\GroupRepository;
use App\Repository\SubjectRepository;
use App\Repository\UserRepository;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *@Route("/admin-student",name="admin_student_")
 */


class StudentController extends AbstractController
{
    public MailerService $sendEmail;
    public UserPasswordEncoderInterface $encoder;

    public function __construct(MailerService $sendEmail, UserPasswordEncoderInterface $encoder)
    {
        $this->sendEmail = $sendEmail;
        $this->encoder = $encoder;
    }
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, GroupRepository $groupRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $groups = $groupRepository->findBy(["admin"=>$this->getUser()]);
        $students = $userRepository->findByRolesAnAdminStudent("ROLE_STUDENT",$this->getUser());
        $nbStudents = count($students);

        return $this->render('admin/student/index.html.twig', [
            'users' => $students,
            'groups' => $groups,
            'nbStudents' => $nbStudents,
            'form'=> $this->createForm(UploadType::class, null)->createView()
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $count_student = $this->getUser()->getStudents()->count();
        $user_pack_limit = $this->getUser()->getPack()->getNbStudents();
        if ($count_student >= $user_pack_limit) {
            $this->addFlash('danger', 'You cannot add a new student, you have reach the maximum limit. If you want to add new students, you must switch to a pack with higher limits.');
            return $this->redirectToRoute('admin_student_index');
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if (!$userRepository->findOneBy(['email' => $user->getEmail()])) {

                // Create password to student
                $user->setPassword($this->generateRandomString(15, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
                $password = $user->getPassword();
                // Encode password
                $encoded = $this->encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //Add ROLE_STUDENT
                $user->setRoles(['ROLE_STUDENT']);

                // add admin
                $user->setAdminStudent($this->getUser());

                $entityManager->persist($user);
                $entityManager->flush();

                $path = $this->generateUrl('app_check_email',[],UrlGeneratorInterface::ABSOLUTE_URL);
                // Send The credentials to student to login
                $this->sendEmail->sendEmail($user);

                $this->addFlash('success', 'Student created successfully');

                return $this->redirectToRoute('admin_student_index');
            }
            $this->addFlash('danger', 'The email is already exists!');
        }

        return $this->render('admin/student/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    public function generateRandomString($length = 6, $characters = '0123456789abcdefghijklmnopqrstuvwxyz')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('student_show', $user);

        return $this->render('admin/student/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('student_edit', $user);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Student updated successfully');

            return $this->redirectToRoute('admin_student_index');
        }

        return $this->render('admin/student/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('student_delete', $user);

        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $this->addFlash('success', 'Student deleted successfully');
        }

        return $this->redirectToRoute('admin_student_index');
    }


    /**
     * @Route("/delete", name="delete_users", methods={"POST"})
     */
    public function deleteUsers(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        if ($this->isCsrfTokenValid('deleteUsers', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $students = $this->getUser()->getStudents();
            $count = 0;
            foreach($students as $student) {
                if (!is_null($request->request->get('form-checkbox-' . $student->getId()))) {
                    $conversations = $student->getConversations();
                    foreach($conversations as $conversation) {
                        $conversation->setLastMessage(null);
                        $conversation->setLastMessageRead([]);
                        foreach($conversation->getMessages() as $message) {
                            $conversation->removeMessage($message);
                            $em->remove($message);
                            $em->flush();
                        }
                        $em->remove($conversation);
                    }
                    $count++;
                    $em->remove($student);
                }
            }
        
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No student has been deleted');
            } else {
                $this->addFlash('success', 'Student(s) deleted successfully');
            }
        }

        return $this->redirectToRoute('admin_student_index');
    }


    /**
     * @Route("/{group}/{student}", name="remove", methods={"DELETE"})
     */
    public function remove(Request $request, Group $group, User $student): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit', $group);
        $this->denyAccessUnlessGranted('student_edit', $student);

        if ($this->isCsrfTokenValid('remove' . $student->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $student->setStudentGroup(null);
            $entityManager->flush();

            $this->addFlash('danger', 'Student removed successfully');
        }
        return $this->redirectToRoute('admin_student_group_index', ['id' => $group->getId()]);
    }



    /**
     * @Route("/import", name="import",methods={"POST"})
     */
    public function importCSV(Request $request,UserRepository $userRepository,EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $admin = $this->getUser();
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $count_student = $admin->getStudents()->count();
            $user_pack_limit = $admin->getPack()->getNbStudents();
            if ($count_student >= $user_pack_limit) {
                $this->addFlash('danger', 'You cannot add a new student, you have reach the maximum limit. If you want to add new students, you must switch to a pack with higher limits.');
                return $this->redirectToRoute('admin_student_index');
            }

            $file = $form['upload_file']->getData();
            if ($file)
            {
                $rowNo = 1;
                if (($fp = fopen($file, "r")) !== FALSE) {
                    while (($row = fgetcsv($fp, 1000, ";")) !== FALSE) {
                        $num = count($row);
                        if ($num === 3){
                            if($row[0] !=="firstName" && $row[1] !=="lastName" && $row[2] !=="email"){
                                $user = new User();

                                $rowNo++;
                                $user->setFirstName($row[0]);
                                $user->setLastName($row[1]);
                                $user->setEmail($row[2]);
                                $entityManager = $this->getDoctrine()->getManager();
                                if (!$userRepository->findOneBy(['email' => $user->getEmail()])) {

                                    // Create password to student
                                    $user->setPassword($this->generateRandomString(15, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
                                    $password = $user->getPassword();
                                    // Encode password
                                    $encoded = $this->encoder->encodePassword($user, $user->getPassword());
                                    $user->setPassword($encoded);

                                    //Add ROLE_STUDENT
                                    $user->setRoles(['ROLE_STUDENT']);

                                    //ADD ADMIN
                                    $user->setAdminStudent($admin);

                                    $entityManager->persist($user);
                                    $entityManager->flush();
                                    $this->sendEmail->sendEmail($user);
                                    $this->addFlash('success', 'Student created successfully');
                                }else{
                                    $this->addFlash('danger', 'The email is already exists!');
                                }
                            }
                        }
                    }
                    fclose($fp);
                }
            }
        }
        return $this->redirectToRoute('admin_student_index');
    }

}
