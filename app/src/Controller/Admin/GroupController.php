<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group",name="group_")
 */
class GroupController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(GroupRepository $groupRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $students = $this->getUser()->getStudents();
        $studentsWithoutGroup = [];
        foreach($students as $student) {
            if (is_null($student->getStudentGroup())) {
                $studentsWithoutGroup[] = $student;
            }
        }
        $professors = $this->getUser()->getProfessors();

        return $this->render('admin/group/index.html.twig', [
            'groups' => $groupRepository->findBy(["admin"=>$this->getUser()]),
            'professors' => $professors,
            'students' => $studentsWithoutGroup,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $groups = $user->getGroups()->toArray();

            foreach($groups as $index => $g) {
                if ($g->getName() != $group->getName()) {
                    unset($groups[$index]);
                }
            }
            if (empty($groups)) {
                $entityManager = $this->getDoctrine()->getManager();
                $group->setAdmin($this->getUser());
                $entityManager->persist($group);
                $entityManager->flush();

                $this->addFlash('success', 'Class created successfully');

                return $this->redirectToRoute('admin_group_index');
            }
            $this->addFlash('danger', 'The class name is already exists!');
        }

        return $this->render('admin/group/new.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_show',$group);
        
        $students = $group->getGroupStudents();
        $professors = $group->getGroupProfessors();

        return $this->render('admin/group/show.html.twig', [
            'group' => $group,
            'students' => $students,
            'professors' => $professors,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);

        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_group_index');
        }

        return $this->render('admin/group/edit.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_delete',$group);

        if ($this->isCsrfTokenValid('delete' . $group->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($group);
            $entityManager->flush();
            $this->addFlash('danger', 'Class deleted successfully');
        }

        return $this->redirectToRoute('admin_group_index');
    }
}
