<?php

namespace App\Controller\Admin;

use App\Entity\Transaction;
use App\Form\TransactionType;
use App\Repository\PackRepository;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transaction",name="transaction_")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(UserRepository $userRepository,PackRepository $packRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $user = $userRepository->findOneBy(['email'=>$this->getUser()->getUsername()]);
        return $this->render('admin/transaction/index.html.twig', [
            'transactions' =>$user->getTransactions(),
            'bill' =>$user->getTransactions()->last(),
            'adminPack' =>$user->getPack(),
            'packs'=> $packRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Transaction $transaction): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('transaction_edit', $transaction);

        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_transaction_index');
        }

        return $this->render('admin/transaction/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }
}
