<?php

namespace App\Controller\Accueil;

use App\Entity\Pack;
use App\Repository\PackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PackRepository $packRepository)
    {
        $offres = $packRepository->findAllOrderByPrice();
        return $this->render('home.html.twig', [
            'offres' => $offres,
        ]);
    }

    /**
     * @Route("/packs", name="packs")
     */
    public function packs(PackRepository $packRepository)
    {
        $packs = $packRepository->findAll();

        return $this->render('home_pack.html.twig', [
            'offres' => $packs,
        ]);
    }

    /**
     * @Route("/home_user", name="home_user")
     */
    public function indexUser()
    {
        $user = $this->getUser();
        if(is_null($user)) {
            return $this->redirectToRoute('app_login');
        }
        if(in_array("ROLE_SUPER_ADMIN", $user->getRoles())) {
            return $this->redirectToRoute('super_admin_index');
        }
        if(in_array("ROLE_ADMIN", $user->getRoles())) {
            return $this->redirectToRoute('admin_index');
        }
        if(in_array("ROLE_PROFESSOR", $user->getRoles())) {
            return $this->redirectToRoute('professor_index');
        }
        if(in_array("ROLE_STUDENT", $user->getRoles())) {
            return $this->redirectToRoute('student_index');
        }
    }
}
