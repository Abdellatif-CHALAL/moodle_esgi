<?php

namespace App\Controller\Professor;

use App\Entity\Exercise;
use App\Entity\Group;
use App\Entity\Question;
use App\Entity\Subject;
use App\Entity\User;
use App\Form\EditExerciseType;
use App\Form\ExerciseType;
use App\Repository\ExerciseRepository;
use App\Repository\GroupRepository;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/exercise",name="exercise_")
 */
class ExerciseController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ExerciseRepository $exerciseRepository, GroupRepository $groupRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $groups = $this->getUser()->getProfessorGroups();
        return $this->render('professor/exercise/index.html.twig', [
            'exercises' => $exerciseRepository->findBy(['subject' => $this->getUser()->getSubject()]),
            'groups' => $groups
        ]);
    }

    /**
     * @Route("/new/{id}", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, ExerciseRepository $exerciseRepository, Subject $subject): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exercise_new", $subject);

        $exercise = new Exercise();
        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$exerciseRepository->checkNameExist($exercise->getName(), $subject)) {
                $entityManager = $this->getDoctrine()->getManager();

                $i = 1;
                $nameFound = true;

                while($nameFound) {
                    $name = $request->request->get('name' . $i);
                    if (is_null($name)) {
                       $nameFound = false;
                    } elseif(empty($name) || empty($request->request->get('description' . $i)) || empty($request->request->get('difficulty' . $i)) || empty($request->request->get('type' . $i))) {
                        $this->addFlash('danger', 'You must to fill all the fields for the questions');

                        return $this->render('professor/exercise/new.html.twig', [
                            'exercise' => $exercise,
                            'form' => $form->createView(),
                        ]);
                    } else {
                        $question = new Question();
                        $question->setName($request->request->get('name' . $i));
                        $question->setDescription($request->request->get('description' . $i));
                        $question->setDifficulty($request->request->get('difficulty' . $i));
                        $question->setType($request->request->get('type' . $i));
                        if($question->getType() == "code"){
                            $functionName = $request->request->get('function-name' . $i);
                            $tests = [];
                            for($j = 1; $j < 4; $j++) {
                                if (!is_null($request->request->get('test' . $j."-".$i)) && !empty($request->request->get('test' . $j."-".$i))) {
                                    if (!is_null($request->request->get('test' . $j."-result-".$i)) && !empty($request->request->get('test' . $j."-result-".$i))) {
                                        $tests[] = array($request->request->get('test' . $j."-".$i), $request->request->get('test' . $j."-result-".$i));
                                    } else {
                                        $tests[] = array($request->request->get('test' . $j."-".$i), '');
                                    }
                                } else {
                                    if (!is_null($request->request->get('test' . $j."-result-".$i)) && !empty($request->request->get('test' . $j."-result-".$i))) {
                                        $tests[] = array('', $request->request->get('test' . $j."-result-".$i));
                                    }
                                }
                            }
                            $element = array($functionName, $tests);
                            $element = json_encode($element);
                            $question->setCorrectAnswer($element);
                        } else {
                            $question->setCorrectAnswer($request->request->get('answer' . $i));
                            $question->addOtherAnswer($request->request->get('answer' . $i));
                            $k = 1;
                            while ($request->request->get('answer' . $k . '-' . $i)) {
                                $question->addOtherAnswer($request->request->get('answer' . $k . '-' . $i));
                                $k++;
                            }
                        }
                        $exercise->addQuestion($question);
                        $entityManager->persist($question);
                    }
                    $i++;
                }

                $exercise->setSubject($subject);
                $entityManager->persist($exercise);
                $entityManager->flush();

                $this->addFlash('success', 'Exercise created successfully');

                return $this->redirectToRoute('professor_exercise_index');
            }

            $this->addFlash('danger', 'This exercise name already exists for this subject !');
        }

        return $this->render('professor/exercise/new.html.twig', [
            'exercise' => $exercise,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Exercise $exercise, QuestionRepository $questionRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exercise_show", $exercise->getSubject());

        $questions = $questionRepository->findByExercise($exercise->getId());
        return $this->render('professor/exercise/show.html.twig', [
            'exercise' => $exercise,
            'questions' => $questions,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Exercise $exercise, ExerciseRepository $exerciseRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exercise_edit", $exercise->getSubject());

        $oldName = $exercise->getName();
        $subject = $this->getUser()->getSubject();

        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$exerciseRepository->checkNameExist($exercise->getName(), $subject) || $exercise->getName() == $oldName) {
                $entityManager = $this->getDoctrine()->getManager();

                $i = 1;
                $nameFound = true;

                while($nameFound) {
                    $name = $request->request->get('name' . $i);
                    if (is_null($name)) {
                       $nameFound = false;
                    } elseif(empty($name) || empty($request->request->get('description' . $i)) || empty($request->request->get('difficulty' . $i)) || empty($request->request->get('type' . $i))) {
                        $this->addFlash('danger', 'You must to fill all the fields for the questions');

                        return $this->render('professor/exercise/new.html.twig', [
                            'exercise' => $exercise,
                            'form' => $form->createView(),
                        ]);
                    } else {
                        $question = new Question();
                        $question->setName($request->request->get('name' . $i));
                        $question->setDescription($request->request->get('description' . $i));
                        $question->setDifficulty($request->request->get('difficulty' . $i));
                        $question->setType($request->request->get('type' . $i));
                        if($question->getType() == "code"){
                            $functionName = $request->request->get('function-name' . $i);
                            $tests = [];

                            for($j = 1; $j < 4; $j++) {
                                if (!is_null($request->request->get('test' . $j)) && !empty($request->request->get('test' . $j))) {
                                    if (!is_null($request->request->get('test' . $j."-result")) && !empty($request->request->get('test' . $j."-result"))) {
                                        $tests[] = array($request->request->get('test' . $j), $request->request->get('test' . $j."-result"));
                                    } else {
                                        $tests[] = array($request->request->get('test' . $j), '');
                                    }
                                } else {
                                    if (!is_null($request->request->get('test' . $j."-result")) && !empty($request->request->get('test' . $j."-result"))) {
                                        $tests[] = array('', $request->request->get('test' . $j."-result"));
                                    }
                                }
                            }
                            $element = array($functionName, $tests);
                            $element = json_encode($element);
                            $question->setCorrectAnswer($element);
                        } else {
                            $question->setCorrectAnswer($request->request->get('answer' . $i));
                            $question->addOtherAnswer($request->request->get('answer' . $i));
                            $k = 1;
                            while ($request->request->get('answer' . $k . '-' . $i)) {
                                $question->addOtherAnswer($request->request->get('answer' . $k . '-' . $i));
                                $k++;
                            }
                        }
                        $exercise->addQuestion($question);
                        $entityManager->persist($question);
                    }
                    $i++;
                }

                $exercise->setSubject($subject);
                $entityManager->persist($exercise);
                $entityManager->flush();

                $this->addFlash('success', 'Exercise updated successfully');

                return $this->redirectToRoute('professor_exercise_index');
            }

            $this->addFlash('danger', 'This exercise name already exists for this subject !');
        }

        return $this->render('professor/exercise/edit.html.twig', [
            'exercise' => $exercise,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Exercise $exercise): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exercise_delete", $exercise->getSubject());

        if ($this->isCsrfTokenValid('delete' . $exercise->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($exercise);
            $entityManager->flush();

            $this->addFlash('danger', 'Exrecise deleted successfully');
        }

        return $this->redirectToRoute('professor_exercise_index');
    }


    /**
     * @Route("/delete", name="delete_exercises", methods={"POST"})
     */
    public function deleteExercises(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        if ($this->isCsrfTokenValid('deleteExercises', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $exercises = $this->getUser()->getSubject()->getExercises();
            $count = 0;
            foreach($exercises as $exercise) {
                if (!is_null($request->request->get('form-checkbox-' . $exercise->getId()))) {
                    $count++;
                    $em->remove($exercise);
                }
            }
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No exercise has been deleted');
            } else {
                $this->addFlash('success', 'Exercise(s) deleted successfully');
            }
        }

        return $this->redirectToRoute('professor_exercise_index');
    }


    /**
     * @Route("/{group}/{exercise}", name="remove", methods={"DELETE"})
     */
    public function remove(Request $request, Group $group, Exercise $exercise): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("group_edit", $group);
        $this->denyAccessUnlessGranted("exercise_delete", $exercise->getSubject());

        if ($this->isCsrfTokenValid('remove' . $exercise->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $exercise->removeGroup($group);
            $entityManager->flush();

            $this->addFlash('danger', 'Exercise removed successfully');
        }
        return $this->redirectToRoute('professor_exercise_group_index', ['id' => $group->getId()]);
    }

    /**
     * @Route("/add-class/{id}", name="addGroup", methods={"POST"})
     */
    public function addGroup(Exercise $exercise, Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exercise_edit", $exercise->getSubject());

        $added = 0;
        $professor = $this->getUser();
        foreach($professor->getProfessorGroups() as $group) {
            if (!is_null($request->get('checkbox-' . $group->getId()))) {
                $exercise->addGroup($group);
                $added++;
            }
        }

        $em->persist($group);
        $em->flush();

        if ($added > 0) {
            $this->addFlash('success', 'Class(es) successfully added');
        } else {
            $this->addFlash('warning', 'No class added');
        }
        
        return $this->redirectToRoute('professor_exercise_index', [
            'id' => $this->getUser()->getId(),
        ]);
    }
}
