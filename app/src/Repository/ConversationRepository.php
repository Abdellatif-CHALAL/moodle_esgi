<?php

namespace App\Repository;

use App\Entity\Conversation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\AST\Join;
use Doctrine\ORM\Query\Expr\Join as ExprJoin;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conversation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversation[]    findAll()
 * @method Conversation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversation::class);
    }


    public function findConversation($otherUser, $currentuser)
    {
        return $this->createQueryBuilder('c')
            ->andWhere(':user MEMBER OF c.participants')
            ->setParameter('user', $currentuser)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllConversations($user) 
    {
        return $this->createQueryBuilder('c')
            ->andwhere(':user MEMBER OF c.participants')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }

    public function checkIfUserisParticipant($conversationId, $user)
    {
        return $this->createQueryBuilder('c')
            ->where('c.id = :conversationId')
            ->andWhere(':user MEMBER OF c.participants')
            ->setParameters([
                'conversationId' => $conversationId,
                'user' => $user
            ])
        ;
    }

    // /**
    //  * @return Conversation[] Returns an array of Conversation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conversation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
