<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(
 *      fields={"email"},
 *      message="This email already exist."
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"message:read", "conversation:read"})
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 1,
     *      max = 30,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *     pattern="/[<>]/",
     *     match=false,
     *     message="Your firstname cannot contain characters '<' or '>'"
     * )
     * @Groups("conversation:read")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 1,
     *      max = 30,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *     pattern="/[<>]+$/i",
     *     match=false,
     *     message="Your firstname cannot contain characters '<' or '>'"
     * )
     * @Groups("conversation:read")
     */
    private $lastName;


    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Your email must be at least {{ limit }} characters long",
     *      maxMessage = "Your email cannot be longer than {{ limit }} characters"
     * )
     *  @Assert\Email(
     *     message = "The email is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;


    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="users", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, inversedBy="groupProfessors")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $professorGroups;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="groupStudents")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $studentGroup;

    /**
     * @ORM\ManyToMany(targetEntity=Exercise::class, inversedBy="solvedBy")
     */
    private $solvedExercises;

    /**
     * @ORM\OneToMany (targetEntity=StudentAnswer::class, mappedBy="student")
     */
    private $studentAnswers;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="admin", orphanRemoval=true)
     */
    private $transactions;

    /**
     * @ORM\ManyToOne(targetEntity=Pack::class)
     */
    private $pack;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adminStudent;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="adminStudent", orphanRemoval=true)
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="professors")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adminProfessor;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="adminProfessor", orphanRemoval=true)
     */
    private $professors;

    /**
     * @ORM\OneToMany(targetEntity=Group::class, mappedBy="admin", orphanRemoval=true)
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity=Subject::class, mappedBy="admin", orphanRemoval=true)
     */
    private $subjects;
    /**
     * @ORM\ManyToMany(targetEntity=Conversation::class, mappedBy="participants")
     */
    private $conversations;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="author", orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToOne(targetEntity=Subject::class, inversedBy="professor")
     */
    private $subject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stripeToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stripeSubscription;



    public function __construct()
    {
        $this->exercises = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->professorGroups = new ArrayCollection();
        $this->solvedExercises = new ArrayCollection();
        $this->transactions = new ArrayCollection();

        $this->students = new ArrayCollection();
        $this->professors = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->subjects = new ArrayCollection();

        $this->conversations = new ArrayCollection();
        $this->messages = new ArrayCollection();

    }




    public function getId(): ?int
    {
        return $this->id;
    }



    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = strtolower($email);

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUsers($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUsers() === $this) {
                $comment->setUsers(null);
            }
        }

        return $this;
    }

    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @return Collection|Group[]
     */
    public function getProfessorGroups(): Collection
    {
        return $this->professorGroups;
    }

    public function addProfessorGroup(Group $professorGroup): self
    {
        if (!$this->professorGroups->contains($professorGroup)) {
            $this->professorGroups[] = $professorGroup;
        }

        return $this;
    }

    public function removeProfessorGroup(Group $professorGroup): self
    {
        if ($this->professorGroups->contains($professorGroup)) {
            $this->professorGroups->removeElement($professorGroup);
        }

        return $this;
    }

    public function getStudentGroup(): ?Group
    {
        return $this->studentGroup;
    }

    public function setStudentGroup(?Group $studentGroup): self
    {
        $this->studentGroup = $studentGroup;

        return $this;
    }

    /**
     * @return Collection|Exercise[]
     */
    public function getSolvedExercises(): Collection
    {
        return $this->solvedExercises;
    }

    public function addSolvedExercise(Exercise $solvedExercise): self
    {
        if (!$this->solvedExercises->contains($solvedExercise)) {
            $this->solvedExercises[] = $solvedExercise;
        }

        return $this;
    }

    public function removeSolvedExercise(Exercise $solvedExercise): self
    {
        if ($this->solvedExercises->contains($solvedExercise)) {
            $this->solvedExercises->removeElement($solvedExercise);
        }

        return $this;
    }

    /**
     * @return Collection|StudentAnswer[]
     */
    public function getStudentAnswers(): Collection
    {
        return $this->studentAnswers;
    }

    public function addStudentAnswer(StudentAnswer $studentAnswer): self
    {
        if (!$this->studentAnswers->contains($studentAnswer)) {
            $this->studentAnswers[] = $studentAnswer;
            $studentAnswer->setQuestion($this);
        }

        return $this;
    }

    public function removeStudentAnswer(StudentAnswer $studentAnswer): self
    {
        if ($this->studentAnswers->contains($studentAnswer)) {
            $this->studentAnswers->removeElement($studentAnswer);
            // set the owning side to null (unless already changed)
            if ($studentAnswer->getQuestion() === $this) {
                $studentAnswer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setAdmin($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getAdmin() === $this) {
                $transaction->setAdmin(null);
            }
        }

        return $this;
    }

    public function getPack(): ?Pack
    {
        return $this->pack;
    }

    public function setPack(?Pack $pack): self
    {
        $this->pack = $pack;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getAdminStudent(): ?self
    {
        return $this->adminStudent;
    }

    public function setAdminStudent(?self $adminStudent): self
    {
        $this->adminStudent = $adminStudent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(self $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setAdminStudent($this);
        }

        return $this;
    }

    public function removeStudent(self $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getAdminStudent() === $this) {
                $student->setAdminStudent(null);
            }
        }

        return $this;
    }

    public function getAdminProfessor(): ?self
    {
        return $this->adminProfessor;
    }

    public function setAdminProfessor(?self $adminProfessor): self
    {
        $this->adminProfessor = $adminProfessor;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getProfessors(): Collection
    {
        return $this->professors;
    }

    public function addProfessor(self $professor): self
    {
        if (!$this->professors->contains($professor)) {
            $this->professors[] = $professor;
            $professor->setAdminProfessor($this);
        }

        return $this;
    }

    public function removeProfessor(self $professor): self
    {
        if ($this->professors->removeElement($professor)) {
            // set the owning side to null (unless already changed)
            if ($professor->getAdminProfessor() === $this) {
                $professor->setAdminProfessor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group)
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->setAdmin($this);
        }
    }

    /**
     * @return Collection|Conversation[]
     */
    public function getConversations(): Collection
    {
        return $this->conversations;
    }

    public function addConversation(Conversation $conversation): self
    {
        if (!$this->conversations->contains($conversation)) {
            $this->conversations[] = $conversation;
            $conversation->addParticipant($this);
        }

        return $this;
    }

    public function removeGroup(Group $group)
    {
        if ($this->groups->removeElement($group)) {
            // set the owning side to null (unless already changed)
            if ($group->getAdmin() === $this) {
                $group->setAdmin(null);
            }
        }
    }
    public function removeConversation(Conversation $conversation): self
    {
        if ($this->conversations->removeElement($conversation)) {
            $conversation->removeParticipant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Subject[]
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
            $subject->setAdmin($this);
        }
        return $this;
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setAuthor($this);
        }

        return $this;
    }
    public function removeSubject(Subject $subject)
    {
        if ($this->subjects->removeElement($subject)) {
            // set the owning side to null (unless already changed)
            if ($subject->getAdmin() === $this) {
                $subject->setAdmin(null);
            }
        }
    }
    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getAuthor() === $this) {
                $message->setAuthor(null);
            }
        }

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getStripeToken(): ?string
    {
        return $this->stripeToken;
    }

    public function setStripeToken(?string $stripeToken): self
    {
        $this->stripeToken = $stripeToken;

        return $this;
    }

    public function getStripeSubscription(): ?string
    {
        return $this->stripeSubscription;
    }

    public function setStripeSubscription(?string $stripeSubscription): self
    {
        $this->stripeSubscription = $stripeSubscription;

        return $this;
    }
}
