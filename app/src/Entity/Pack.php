<?php

namespace App\Entity;

use App\Repository\PackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PackRepository::class)
 */
class Pack
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbStudents;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrProfessors;

    /**
     * @ORM\Column(type="boolean")
     */
    private $promotion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $free_duration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_duration;

    /**
     * @ORM\Column(type="float")
     */
    private $oldPrice;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="pack", orphanRemoval=true)
     */
    private $transtions;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $stripePrice;

    public function __construct()
    {
        $this->transtions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getNbStudents(): ?int
    {
        return $this->nbStudents;
    }

    public function setNbStudents(int $nbStudents): self
    {
        $this->nbStudents = $nbStudents;

        return $this;
    }

    public function getNbrProfessors(): ?int
    {
        return $this->nbrProfessors;
    }

    public function setNbrProfessors(int $nbrProfessors): self
    {
        $this->nbrProfessors = $nbrProfessors;

        return $this;
    }

    public function getPromotion(): ?bool
    {
        return $this->promotion;
    }

    public function setPromotion(bool $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }

    public function getFreeDuration(): ?int
    {
        return $this->free_duration;
    }

    public function setFreeDuration(?int $free_duration): self
    {
        $this->free_duration = $free_duration;

        return $this;
    }

    public function getTypeDuration(): ?string
    {
        return $this->type_duration;
    }

    public function setTypeDuration(?string $type_duration): self
    {
        $this->type_duration = $type_duration;

        return $this;
    }

    public function getOldPrice(): ?float
    {
        return $this->oldPrice;
    }

    public function setOldPrice(float $oldPrice): self
    {
        $this->oldPrice = $oldPrice;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTranstions(): Collection
    {
        return $this->transtions;
    }

    public function addTranstion(Transaction $transtion): self
    {
        if (!$this->transtions->contains($transtion)) {
            $this->transtions[] = $transtion;
            $transtion->setPack($this);
        }

        return $this;
    }

    public function removeTranstion(Transaction $transtion): self
    {
        if ($this->transtions->removeElement($transtion)) {
            // set the owning side to null (unless already changed)
            if ($transtion->getPack() === $this) {
                $transtion->setPack(null);
            }
        }

        return $this;
    }

    public function getStripePrice(): ?string
    {
        return $this->stripePrice;
    }

    public function setStripePrice(string $stripePrice): self
    {
        $this->stripePrice = $stripePrice;

        return $this;
    }
}
