<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class AdminUserVoter extends Voter
{

    const PROFESSOR_SHOW = 'professor_show';
    const PROFESSOR_EDIT= 'professor_edit';
    const PROFESSOR_DELETE = 'professor_delete';

    const STUDENT_SHOW = 'student_show';
    const STUDENT_EDIT= 'student_edit';
    const STUDENT_DELETE = 'student_delete';


    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $professor)
    {
        return in_array($attribute, [self::PROFESSOR_SHOW,self::PROFESSOR_EDIT,self::PROFESSOR_DELETE,self::STUDENT_SHOW,self::STUDENT_EDIT,self::STUDENT_DELETE])
            && ($professor instanceof \App\Entity\User);
    }

    protected function voteOnAttribute($attribute, $user, TokenInterface $token)
    {
        $currentUser = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$currentUser instanceof UserInterface) {
            return false;
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return false;
        }

        switch ($attribute) {
            case self::PROFESSOR_SHOW:
                return $this->canChangeProfessor($user, $currentUser);
                break;
            case self::PROFESSOR_EDIT:
                return $this->canChangeProfessor($user, $currentUser);
                break;
            case self::PROFESSOR_DELETE:
                return $this->canChangeProfessor($user, $currentUser);
                break;
            case self::STUDENT_SHOW:
                return $this->canChangeStudent($user, $currentUser);
                break;
            case self::STUDENT_EDIT:
                return $this->canChangeStudent($user, $currentUser);
                break;
            case self::STUDENT_DELETE:
                return $this->canChangeStudent($user, $currentUser);
                break;
        }

        return false;
    }

    private function canChangeProfessor(User $professor, User $user)
    {
        return !is_null($professor->getAdminProfessor()) && $user === $professor->getAdminProfessor();
    }

    private function canChangeStudent(User $student, User $user)
    {
        return !is_null($student->getAdminStudent()) && $user === $student->getAdminStudent();
    }
    
}