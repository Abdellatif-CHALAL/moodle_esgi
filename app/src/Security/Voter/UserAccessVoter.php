<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class UserAccessVoter extends Voter
{

    const ACCESS_MENU = 'access_menu';
    const CLASS_PROFESSOR_ADD = 'class_professor_add';
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $user)
    {
        return in_array($attribute, [self::ACCESS_MENU, self::CLASS_PROFESSOR_ADD])
            && $user instanceof \App\Entity\User;
    }

    protected function voteOnAttribute($attribute, $user, TokenInterface $token)
    {
        $u = $token->getUser();

        if (!$u instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::ACCESS_MENU:
                return $this->canView($user);
                break;
            case self::CLASS_PROFESSOR_ADD:
                return $this->canAdd($user, $u);
                break;
        }

        return false;
    }

    private function canView(User $user)
    {
        $roles = $user->getRoles();
        $user_role = $roles[0];

        if ($user_role == "ROLE_ADMIN") {
            if (!$user->isVerified()) {
                return false;
            }
            return $user->getPack() != null;
        } elseif ($user_role == "ROLE_PROFESSOR") {
            return $user->getAdminProfessor()->getPack() != null;
        } elseif ($user_role == "ROLE_STUDENT") {
            return $user->getAdminStudent()->getPack() != null;
        } else {
            return false;
        }
    }

    private function canAdd(User $professor, User $user)
    {
        return (!is_null($professor->getAdminProfessor()) && $professor->getAdminProfessor() === $user);
    }
}
