<?php

namespace App\Security\Voter;

use App\Entity\Subject;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class ExerciseVoter extends Voter
{

    const EXERCISE_NEW = 'exercise_new';
    const EXERCISE_SHOW = 'exercise_show';
    const EXERCISE_EDIT = 'exercise_edit';
    const EXERCISE_DELETE = 'exercise_delete';
    
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
    
        return in_array($attribute, [self::EXERCISE_NEW, self::EXERCISE_SHOW, self::EXERCISE_EDIT, self::EXERCISE_DELETE])
            && $subject instanceof \App\Entity\Subject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($subject->getProfessor() === null) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EXERCISE_NEW:
                return $this->canCreate($subject, $user);
                break;
            case self::EXERCISE_SHOW:
                return $this->canShow($subject, $user);
                break;
            case self::EXERCISE_EDIT:
                return $this->canEdit($subject, $user);
                break;
            case self::EXERCISE_DELETE:
                return $this->canDelete($subject, $user);
                break;
        }

        return false;
    }

    private function canCreate(Subject $subject, User $user)
    {
        return $subject->getProfessor() == $user;
    }

    private function canShow(Subject $subject, User $user)
    {
        return $subject->getProfessor() == $user;
    }

    private function canEdit(Subject $subject, User $user)
    {
        return $subject->getProfessor() == $user;
    }

    private function canDelete(Subject $subject, User $user)
    {
        return $subject->getProfessor() == $user;
    }
}
