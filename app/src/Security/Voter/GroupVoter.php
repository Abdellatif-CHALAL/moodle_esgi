<?php

namespace App\Security\Voter;

use App\Entity\Group;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class GroupVoter extends Voter
{

    const GROUP_SHOW = 'group_show';
    const GROUP_EDIT = 'group_edit';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $element)
    {
      
        return in_array($attribute, [self::GROUP_SHOW, self::GROUP_EDIT])
            && ($element instanceof \App\Entity\User || $element instanceof \App\Entity\Group);
    }

    protected function voteOnAttribute($attribute, $element, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($element === null) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::GROUP_SHOW:
                return $this->canShow($element, $user);
                break;
            case self::GROUP_EDIT:
                return $this->canEdit($element, $user);
                break;
        }

        return false;
    }

    private function canShow(User $professor, User $user)
    {
        return $user === $professor;
    }

    private function canEdit(Group $group, User $user)
    {
        return $group->getGroupProfessors()->contains($user);
    }

}
