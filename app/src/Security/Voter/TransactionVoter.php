<?php

namespace App\Security\Voter;

use App\Entity\Transaction;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class TransactionVoter extends Voter
{

    const TRANSACTION_EDIT = 'transaction_edit';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $transaction)
    {
        return in_array($attribute, [self::TRANSACTION_EDIT])
            && ($transaction instanceof \App\Entity\Transaction);
    }

    protected function voteOnAttribute($attribute, $transaction, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return false;
        }

        switch ($attribute) {
            case self::PROFESSOR_SHOW:
                return $this->canChange($user, $transaction);
                break;
        }

        return false;
    }

    private function canChange(User $user, Transaction $transaction)
    {
        return !is_null($transaction->getAdmin()) && $user === $transaction->getAdmin();
    }
    
}