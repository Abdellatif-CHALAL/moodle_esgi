<?php

namespace App\Security\Voter;

use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class StudentGroupVoter extends Voter
{

    const STUDENTS_SHOW = 'studentGroup_show';
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $professors)
    {
       
        return in_array($attribute, [self::STUDENTS_SHOW])
            && $professors instanceof Collection;
    }

    protected function voteOnAttribute($attribute, $professors, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::STUDENTS_SHOW:
                return $this->canShow($professors, $user);
                break;
        }

        return false;
    }

    private function canShow(Collection $professors, User $user)
    {
        return $professors->contains($user);
    }

}
