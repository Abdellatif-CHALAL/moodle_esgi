<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class SuperAdminVoter extends Voter
{

    const SUPER_ADMIN = 'access_super_admin';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $user)
    {
        return in_array($attribute, [self::SUPER_ADMIN])
            && $user instanceof User;
    }

    protected function voteOnAttribute($attribute, $user, TokenInterface $token)
    {
        $u = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$u instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::SUPER_ADMIN:
                return $this->canAccess($u, $user);
                break;
        }

        return false;
    }

    private function canAccess(User $u, User $user)
    {
        $roles = $user->getRoles();
        return $u === $user && in_array("ROLE_SUPER_ADMIN", $roles);
    }

}
