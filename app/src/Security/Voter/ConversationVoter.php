<?php

namespace App\Security\Voter;

use App\Services\UserService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ConversationVoter extends Voter
{
    const ADD = 'conversation_add';
    const VIEW = 'conversation_view';
    const CREATE = 'message_create';

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::ADD, self::VIEW, self::CREATE])
            && ($subject instanceof \App\Entity\User || $subject instanceof \App\Entity\Conversation);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::ADD:
                return $this->canAdd($user, $subject);
                break;
            case self::VIEW:
                return $this->canView($user, $subject);
                break;
            case self::CREATE:
                return $this->canCreate($user, $subject);
        }
        return false;
    }

    private function canAdd($user, $subject) {
        if (in_array("ROLE_STUDENT", $user->getRoles())) {
            return $this->userService->findConversationWithProfessor($user, $subject);
        }
        if (in_array("ROLE_PROFESSOR", $user->getRoles())) {
            return $this->userService->findConversationWithStudent($user, $subject);
        }
    }

    private function canView($user, $conversation) {
        if ($conversation->getParticipants()->contains($user)) {
            return true;
        }
        return false;
    }

    private function canCreate($user, $conversation) {
        return $conversation->getParticipants()->contains($user) ? true : false;
    }
        
}