<?php

namespace App\Security\Voter;

use App\Entity\Exercise;
use App\Entity\Subject;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class ExerciseStudentVoter extends Voter
{

    const EXERCISESTUDENT_SHOW = 'exerciseStudent_show';
    const EXERCISESTUDENT_NEW = 'exerciseStudent_new';
    const EXERCISEPROFESSOR_NEW = 'exerciseProfessor_new';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $groups)
    {
        return in_array($attribute, [self::EXERCISESTUDENT_SHOW, self::EXERCISESTUDENT_NEW, self::EXERCISEPROFESSOR_NEW])
            && ($groups instanceof Collection || $groups instanceof Exercise);
    }

    protected function voteOnAttribute($attribute, $groups, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EXERCISESTUDENT_SHOW:
                return $this->canShow($groups, $user);
                break;
            case self::EXERCISESTUDENT_NEW:
                return $this->canCreate($groups, $user);
                break;
            case self::EXERCISEPROFESSOR_NEW:
                return $this->canCreateComment($groups, $user);
                break;
        }

        return false;
    }

    private function canShow(Collection $groups, User $user)
    {
        foreach($groups as $group) {
            if ($group->getGroupStudents()->contains($user)) return true;
        }
        return false;
    }

    private function canCreate(Collection $groups, User $user)
    {
        foreach($groups as $group) {
            if ($group->getGroupStudents()->contains($user)) return true;
            if ($group->getGroupProfessors()->contains($user)) return true;
        }
        return false;
    }

    private function canCreateComment(Exercise $groups, User $user)
    {
        return $groups->getSubject()->getProfessor() == $user;
    }

}
