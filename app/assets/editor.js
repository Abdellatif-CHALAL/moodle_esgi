/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import 'bootstrap';
import ace from 'brace'
import 'brace/mode/html'
import 'brace/mode/php'
import 'brace/theme/monokai'



$(function() {
    $('.btn-code').on('click', function(){
        let tmp = editor.getSession().getValue();
        let test1 = eval(tmp+$('#function-name').html()+"("+$('#test1').html()+");");
        let test2 = eval(tmp+$('#function-name').html()+"("+$('#test2').html()+");");
        let test3 = eval(tmp+$('#function-name').html()+"("+$('#test3').html()+");");
        $("#test1-result").html(test1);
        $("#test2-result").html(test2);
        $("#test3-result").html(test3);

    });
    let editor = ace.edit("test");
    editor.session.setMode('ace/mode/javascript');
    editor.setTheme('ace/theme/monokai');
    let result = $(".result");
    let resultEvaluated = $(".result-evaluated");

    editor.session.setValue("function "+$('#function-name').html()+"(params){}");
    $("#reset-code-btn").on('click', function (){
        editor.session.setValue("function "+$('#function-name').html()+"(params){}");
    });

    editor.on("change", function () {
        result.val(editor.getSession().getValue());
        try {
            let code = editor.getSession().getValue();
            let test1 = eval(code+$('#function-name').html()+"("+$('#test1').html()+");");
            let test2 = eval(code+$('#function-name').html()+"("+$('#test2').html()+");");
            let test3 = eval(code+$('#function-name').html()+"("+$('#test3').html()+");");
            $("#test1-result").val(test1);
            $("#test2-result").val(test2);
            $("#test3-result").val(test3);
            $(".error-message-code").hide();
            $("#submit-answers-btn").attr("disabled", false);
        } catch (e) {
                $(".error-message-code").show();
                $("#submit-answers-btn").attr("disabled", true);
        }
    });
});
