/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import 'highlight.js/styles/monokai.css';

// // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import 'bootstrap';
import hljs from "highlight.js";


$(function() {
    hljs.highlightAll();

    let globalLastId = '';
    let globalCurrentConversationId = '';
    let globalLastConversationRead = 0;

    $('form').delegate('.type-select', 'change', function (){
        let selectId = $(this).data('id');

        if($(this).val()=="qcm"){
            $(".qcm-libre-form" + selectId).show();
            $(".other-answers" + selectId).show();
            $(".btn-add-answer" + selectId).show();
            $(".code-form" + selectId).hide();

        }else{
            if($(this).val()=="libre") {
                $(".qcm-libre-form" + selectId).show();
                $(".added-answers" + selectId).html('');
                $(".other-answers" + selectId).hide();
                $(".btn-add-answer" + selectId).hide();
                $(".code-form" + selectId).hide();

            }else{if($(this).val()=="code") {
                $(".qcm-libre-form" + selectId).hide();
                $(".code-form" + selectId).show();
            }
            }
        }
    });


    let i = [];
    i[1] = 1;
    let j = 1;

    $('form').delegate('.btn-add-answer', 'click', function(e){
        e.preventDefault();
        let id = $(this).data('id');
        i[id]++;
        $('.added-answers' + id).append("<label>Choice "+i[id]+" :</label>" +
            "<input name='answer"+i[id]+"-"+id+"' type='text' class='form-control'>"
        );
    });

    $('.button-add-new-question').on('click', function(e) {
        e.preventDefault();
        i[j] = 1;
        $('.space-new-question').append("<div class='form-group-question'><h3>Question " + j + "</h3><div class='form-group'><label>Name :</label><input name='name" + j + "' type='text' class='form-control' required></div><div class='form-group'><label>Description :</label><span class='blockquote-footer'>write a description to explain to the students what to do</span><input name='description" + j + "' type='text' class='form-control' required></div><div class='row'><div class='form-group col-6'><label>Difficulty :</label><span class='blockquote-footer'>choose the difficulty to indicate to the students</span><select name='difficulty" + j + "' type='text' class='form-control type-select' required><option value='easy'>Easy</option><option value='normal'>Normal</option><option value='hard'>Hard</option></select></div><div class='form-group col-6'><label>Type :</label><span class='blockquote-footer'>choose the type of the question</span><select name='type" + j + "' type='text' class='form-control type-select' data-id='" + j + "' required><option value='qcm'>QCM</option><option value='libre'>Libre</option><option value='code'>Code Javascript</option></select></div></div><div class='form-group'><div class='qcm-libre-form" + j + "'><label>Expected Answer :</label><span class='blockquote-footer'>indicate the expected answer from students</span><input name='answer" + j + "' type='text' class='form-control' ><br><br><div class='other-answers" + j + "'><h4>Choices for the student :</h4><label>Choice 1 :</label><input name='answer1-" + j + "' type='text' class='form-control'><div class='added-answers" + j + "'></div></div><div class='btn btn-outline-secondary btn-add-answer btn-add-answer" + j + "' data-id='" + j + "' style='margin : 10px 0 10px 0;'>Add answer</div><br></div></div><div class='form-group'><div class='code-form" + j + "' style='display: none;'><div class='form-group'><label>Function name :</label><span class='blockquote-footer'>indicate the name of the function to write by the students</span><input name='function-name" + j + "' type='text'></div><label class='test-title'>Tests :</label><div class='code-instructions'><h6>How to write tests :</h6><ul><li> Write the input parameters on the left, separated by a comma, strings must be surrounded by double quotes. Example : <b>6,[12,15],'Your string'</b></li><li>On the inputs on the right, write the expected output for the parameters on the right,for booleans you can type <b>true</b> or <b>false</b>. Strings here must not be surroundedby quotes.</li></ul></div><div class='form-group'><label class='test-title-small'>Test 1 :</label><div class='row px-5'><div class='col-md-6'><label>Input</label><input name='test1-" + j + "' type='text'></div><div class='col-md-6'><label>Output</label><input name='test1-result-" + j + "' type='text'></div></div></div><div class='form-group'><label class='test-title-small'>Test 2 :</label><div class='row px-5'><div class='col-md-6'><label>Input</label><input name='test2-" + j + "' type='text'></div><div class='col-md-6'><label>Output</label><input name='test2-result-" + j + "' type='text'></div></div></div><div class='form-group'><label class='test-title-small'>Test 3 :</label><div class='row px-5'><div class='col-md-6'><label>Input</label><input name='test3-" + j + "' type='text'></div><div class='col-md-6'><label>Output</label><input name='test3-result-" + j + "' type='text'></div></div></div></div></div></div>");
        j++;
    });

    $('#pack_promotion').on('click', function() {
        $('#details_promotion').toggle();
    });

    $('#formComment').on('submit', function(e){
        e.preventDefault();
        let dataForm = $(this).serialize();
        let pathEdit = $('.pathForEdition').attr("data-src");
        let pathDelete = $('.pathForDeletion').attr("data-src");
        let token = $('.tokenId').attr("data-token");

        $.ajax({
            url : $('#formComment').attr('action'),
            type : "POST",
            data: dataForm,
            success : function(response){
                $('#comments').prepend('<div id="comment' + response.commentId + '"><hr><div id="actionEditedMessage' + response.commentId + '" class="alert alert-success w-50" style="display:none;" role="alert">edited</div><div id="actionDeletedMessage' + response.commentId + '" class="alert alert-danger w-50" style="display:none;" role="alert">deleted</div><div class="commentContent my-4" style="border-left: 1px solid grey; padding: 0px 30px;"><strong class="p-1 text-light bg-secondary">' + response.userfirstname + ' ' + response.userlastname + '</strong><p class="d-inline px-3 light-black"> - </p><p class="d-inline pr-3 light-black">' + response.createdAt + '</p><div class="comment_action_button d-inline light-black"><p class="d-inline"> - </p><a href="#" class="commentEditButton d-inline mx-2" data-id=' + response.commentId + '><u>edit</u></a><a href="#deleteModal' + response.commentId + '" class="d-inline mx-2" data-toggle="modal"><u>delete</u></a></div><p id="commentText' + response.commentId + '" class="pt-2 pb-1">' + response.comment + '</p><div id="divFormEditComment' + response.commentId + '" style="display:none;"><form method="post" action="' + pathEdit + '?id=' + response.commentId + '" id="formCommentEdit' + response.commentId + '"><div class="form-group row"><textarea type="text" class="commentEditInput col-8 form-control" name="commentEditInput" required ></textarea><input class="commentId" type="hidden" name="commentId" value="' + response.commentId + '" /><button class="submitEditButton col-2 btn btn-primary ml-3" style="max-height:40px;" data-id=' + response.commentId + '>edit</button></div></form></div><div class="modal fade" id="deleteModal' + response.commentId + '" tabindex="-1" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><h5>Validation</h5><br /><p>Please confirm the deletion of your comment</p></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="commentDeleteButton btn btn-primary" data-id=' + response.commentId + ' data-src=' + pathDelete + ' data-token="' + token + '">Confirm</button></div></div></div></div></div>');
                $('#commentInput').val("");
                $('#message_no_comment').css('display', 'none');
            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }
        });
    });

    $('#comments').delegate('.submitEditButton', 'click', function(e) {
        e.preventDefault();
        let id = $(this).attr("data-id");
        $('#formCommentEdit' + id).on('submit', function(e) {
            e.preventDefault();
            let dataForm = $(this).serialize();
            let ajaxUrl = $('#formCommentEdit' + id).attr('action');

            $.ajax({
                url : ajaxUrl,
                type : "POST",
                data: dataForm,
                success : function(response){
                    $('#commentEditInput').val("");
                    $('#commentText' + response.commentId).text(response.comment).toggle();
                    $('#divFormEditComment' + response.commentId).toggle();
                    $('.commentEditButton').text("edit");
                    $('#formComment button').prop('disabled', false);
                    $('#actionEditedMessage' + response.commentId).stop().slideDown(function() {
                        setTimeout(function() {
                            $('#actionEditedMessage' + response.commentId).stop().slideUp();
                        }, 1500);
                    });
                },

                error : function(resultat, statut, erreur){
                    
                },

                complete : function(resultat, statut){

                }
            });
        });
        $('#formCommentEdit' + id).trigger('submit');
    });

    $('#comments').delegate('.commentEditButton', 'click', function(e) {
        e.preventDefault();
        if ($(this).text() === "edit") {
            $(this).text("cancel editing");
            $('#formComment button').prop('disabled', true);
        } else {
            $(this).text("edit");
            $('#formComment button').prop('disabled', false);
        }
        let id = $(this).attr("data-id");
        let text = $('#commentText' + id).text();
        $('#divFormEditComment' + id + ' textarea').val(text);
        $('#commentText' + id).stop().toggle();
        $('#divFormEditComment' + id).stop().toggle();
    });

    $('#comments').delegate('.commentDeleteButton', 'click', function(e) {
        e.preventDefault();
        let id = $(this).attr("data-id");
        let token = $(this).attr("data-token");
        let ajaxUrl = $(this).attr("data-src");

        $('#deleteModal' + id).hide();
        $('.modal-backdrop').remove();

        $.ajax({
            url : ajaxUrl,
            type : "DELETE",
            data: 'commentId=' + id + '&token=' + token,
            success : function(response){
                $('.commentContent' + response.commentId).remove();
                $('#actionDeletedMessage' + response.commentId).stop().slideDown(function() {
                    setTimeout(function() {
                        $('#actionDeletedMessage' + response.commentId).stop().slideUp();
                        $('#comment' + response.commentId).remove();
                    }, 1500);
                });
            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }
        });
    });

    $('.submitButtonFormModal').on('click', function(e) {
        e.preventDefault();
        let group = $(e.target).data('group');
        $('#modalFormAddExercises' + group).submit();
    });

    $('.submitButtonFormModalAddClass').on('click', function(e) {
        e.preventDefault();
        let exercise = $(e.target).data('exercise');
        $('#modalFormAddClass' + exercise).submit();
    });

    $('.submitButtonFormModalAddStudents').on('click', function(e) {
        e.preventDefault();
        let group = $(e.target).data('group');
        $('#modalFormAddStudents' + group).submit();
    });

    $('.submitButtonFormModalAddProfessors').on('click', function(e) {
        e.preventDefault();
        let group = $(e.target).data('group');
        $('#modalFormAddProfessors' + group).submit();
    });

    $('#selectAboveAddClasses').on('click', function(e) {
        e.preventDefault();
        $('#selectFormAddClasses').stop().slideToggle();
    })

    $('#selectFormAddClasses option').mousedown(function(e) {
        e.preventDefault();
        var originalScrollTop = $(this).parent().scrollTop();
        console.log(originalScrollTop);
        $(this).prop('selected', $(this).prop('selected') ? false : true);
        var self = this;
        $(this).parent().focus();
        setTimeout(function() {
            $(self).parent().scrollTop(originalScrollTop);
        }, 0);
        
        return false;
    });

    $('#checkbox-all').on('change', function(e) {
        let id = $(e.target).data('id');
        let totalCheckboxes = $('table .form-check-remove input:checkbox').length;
        if(this.checked) {
            $('#remove-checkbox-button').stop().fadeIn();
            $('#' + id + ' input:checkbox').prop('checked', true);
            $('#remove-checkbox-button .count-checkbox').html(totalCheckboxes);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#myTable .modal-body form').append('<input type="hidden" value="" id="form-' + idcheckboxLoop + '" name="form-' + idcheckboxLoop + '"></input>');
            });
        } else {
            $('#remove-checkbox-button').stop().fadeOut();
            $('#' + id + ' input:checkbox').prop('checked', false);
            $('#remove-checkbox-button .count-checkbox').html(0);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#form-' + idcheckboxLoop).remove();
            });
        }
    });

    $('#checkbox-all1').on('change', function(e) {
        let id = $(e.target).data('id');
        let totalCheckboxes = $('.table1 .form-check-remove input:checkbox').length;
        if(this.checked) {
            $('#remove-checkbox-button1').stop().fadeIn();
            $('#' + id + ' input:checkbox').prop('checked', true);
            $('#remove-checkbox-button1 .count-checkbox').html(totalCheckboxes);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#myTable1 .modal-body form').append('<input type="hidden" value="" id="form-' + idcheckboxLoop + '" name="form-' + idcheckboxLoop + '"></input>');
            });
        } else {
            $('#remove-checkbox-button1').stop().fadeOut();
            $('#' + id + ' input:checkbox').prop('checked', false);
            $('#remove-checkbox-button1 .count-checkbox').html(0);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#form-' + idcheckboxLoop).remove();
            });
        }
    });

    $('#checkbox-all2').on('change', function(e) {
        let id = $(e.target).data('id');
        let totalCheckboxes = $('.table2 .form-check-remove input:checkbox').length;
        if(this.checked) {
            $('#remove-checkbox-button2').stop().fadeIn();
            $('#' + id + ' input:checkbox').prop('checked', true);
            $('#remove-checkbox-button2 .count-checkbox').html(totalCheckboxes);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#myTable2 .modal-body form').append('<input type="hidden" value="" id="form-' + idcheckboxLoop + '" name="form-' + idcheckboxLoop + '"></input>');
            });
        } else {
            $('#remove-checkbox-button2').stop().fadeOut();
            $('#' + id + ' input:checkbox').prop('checked', false);
            $('#remove-checkbox-button2 .count-checkbox').html(0);
            $('tbody input:checkbox').each(function() {
                let idcheckboxLoop = $(this).attr('id');
                $('#form-' + idcheckboxLoop).remove();
            });
        }
    });

    $('.table tbody .form-check-input').on('change', function(e) {
        $('#remove-checkbox-button').stop().fadeIn();
        let checkboxeChecked = $('table input:checkbox:checked').length;
        if($('#checkbox-all:checked').is(':checked')) {
            $('#remove-checkbox-button .count-checkbox').html(checkboxeChecked - 1);
        } else {
            $('#remove-checkbox-button .count-checkbox').html(checkboxeChecked);
        }
        if(checkboxeChecked == 0) {
            $('#remove-checkbox-button').stop().fadeOut();
        }
        let idSelected = $(e.target).attr('id');
        if(this.checked) {
            $('#myTable .modal-body form').append('<input type="hidden" value="" id="form-' + idSelected + '" name="form-' + idSelected + '"></input>');
        } else {
            $('#form-' + idSelected).remove();
        }
    });

    $('.table1 tbody .form-check-input').on('change', function(e) {
        $('#remove-checkbox-button1').stop().fadeIn();
        let checkboxeChecked = $('.table1 input:checkbox:checked').length;
        if($('#checkbox-all1:checked').is(':checked')) {
            $('#remove-checkbox-button1 .count-checkbox').html(checkboxeChecked - 1);
        } else {
            $('#remove-checkbox-button1 .count-checkbox').html(checkboxeChecked);
        }
        if(checkboxeChecked == 0) {
            $('#remove-checkbox-button1').stop().fadeOut();
        }
        let idSelected = $(e.target).attr('id');
        if(this.checked) {
            $('#myTable1 .modal-body form').append('<input type="hidden" value="" id="form-' + idSelected + '" name="form-' + idSelected + '"></input>');
        } else {
            $('#form-' + idSelected).remove();
        }
    });

    $('.table2 tbody .form-check-input').on('change', function(e) {
        $('#remove-checkbox-button2').stop().fadeIn();
        let checkboxeChecked = $('.table2 input:checkbox:checked').length;
        if($('#checkbox-all2:checked').is(':checked')) {
            $('#remove-checkbox-button2 .count-checkbox').html(checkboxeChecked - 1);
        } else {
            $('#remove-checkbox-button2 .count-checkbox').html(checkboxeChecked);
        }
        if(checkboxeChecked == 0) {
            $('#remove-checkbox-button2').stop().fadeOut();
        }
        let idSelected = $(e.target).attr('id');
        if(this.checked) {
            $('#myTable2 .modal-body form').append('<input type="hidden" value="" id="form-' + idSelected + '" name="form-' + idSelected + '"></input>');
        } else {
            $('#form-' + idSelected).remove();
        }
    });

    $('.confirmButton-modalFormRemoveProfessors').on('click', function(e) {
        $('#modalFormRemoveProfessors').submit();
    });

    $('.confirmButton-modalFormRemoveStudents').on('click', function(e) {
        $('#modalFormRemoveStudents').submit();
    });

    $('.confirmButton-modalFormRemoveSubjects').on('click', function(e) {
        $('#modalFormRemoveSubjects').submit();
    });

    $('.confirmButton-modalFormRemoveExercises').on('click', function(e) {
        $('#modalFormRemoveExercises').submit();
    });

    $('.button-open-chat div').on('click', function() {
        $('.chat-box').css('display', 'block');
        $('.button-open-chat').css('display', 'none');
        $('.modal-custom').css('display', 'block');
    });
    $('.button-close-chat').on('click', function() {
        $('.chat-box').css('display', 'none');
        $('.button-open-chat').css('display', 'block');
        $('.modal-custom').css('display', 'none');
    });

    $('.create-conversation-button').on('click', function() {
        $('.user-list-creation').stop().slideToggle();
    });

    $.ajax({
        url : '/chat/init',
        type : "POST",
        data: 'token=' + $('.tokenId').attr("data-token"),
        success : function(response){
            if (response.length != 0) {
                if (response.conversations != null && response.conversations.length > 0) {
                    $('.no-conversation').css('display', 'none');
                    
                    response.conversations.forEach(function(conversation) {
                        let userToDisplay = response.usersToDisplay[conversation.id];
                        let lastMessage = response.lastMessage[conversation.id];
                        let globalLastMessageRead = response.lastMessageRead[conversation.id];
                        let lastMessageIdFromConversation = response.lastMessageId[conversation.id];
                        let subtext = '';
                        let badge = '';
                        if (lastMessage == '') {
                            subtext = '<i>no message</i>';
                        } else {
                            if (lastMessage.length > 50) {
                                lastMessage = lastMessage.slice(0, 50) + '...';
                            }
                            subtext = lastMessage;
                        }
                        if (globalLastMessageRead < lastMessageIdFromConversation) {
                            badge = 'new';
                        }
                        if (globalLastConversationRead < conversation.id) {
                            globalLastConversationRead = conversation.id;
                        }
                        $('.list-group-conversation').prepend('<li id="list-group-item-' + conversation.id + '" class="list-group-item d-flex justify-content-between pr-1 pl-2" data-id="' + conversation.id + '"><div class="w-70"><b class="d-flex w-100 m-0">' + userToDisplay + '</b><p class="d-flex w-100 m-0 text-muted">' + subtext + '</p></div><span id="conversation-badge' + conversation.id + '" class="conversation-badge badge badge-pill">' + badge + '</span></li>');
                    });
                    globalLastId = response.globalLastMessage;
                }
                if (response.users_available.length == 0) {
                    $('.user-list-creation').append('<p class="py-3">No user available for new conversation</p>');
                } else {
                    response.users_available.forEach(function(user) {
                        $('.user-list-creation').append('<a href="#" class="list-group-item list-group-item-action" data-src="/conversations/new" data-user="' + user.id + '"> + <small>' + user.firstName + ' ' + user.lastName + '</small></a>');
                    });
                }
            }
        },

        error : function(resultat, statut, erreur){

        },

        complete : function(resultat, statut){

        }
    });

    $('.user-list-creation').on('click', '.list-group-item', function(e){
        e.preventDefault();
        let path = $(this).attr('data-src');
        let userId = $(this).attr('data-user');
        let token = $('.tokenId').attr("data-token");

        $.ajax({
            url : path,
            type : "POST",
            data: 'otherUser=' + userId + '&token=' + token,
            success : function(response){
                var elt = '<li id="list-group-item-' + response.conversationId + '" class="list-group-item d-flex justify-content-between pr-1 pl-2" data-id="' + response.conversationId + '"><div class="w-70"><b class="d-flex w-100 m-0">' + response.userToDisplay + '</b><p class="d-flex w-100 m-0 text-muted"><i>no message</i></p></div><span id="conversation-badge' + response.conversationId + '" class="conversation-badge badge badge-pill"></span></li>';
                
                $('.no-conversation').css('display', 'none');
                $('.chat-box .left-chat .list-group-conversation').prepend(elt);
                $('.chat-box .no-conversation').text('');
                $('.chat-box .left-chat .list-group-conversation li').first().trigger('click');

                globalCurrentConversationId = response.conversationId;
            },

            error : function(resultat, statut, erreur){
                $('.chat-box .left-chat .error-message').text(resultat.responseJSON.message);
                setTimeout(function() {
                    $('.chat-box .left-chat .error-message').text('');
                }, 5000);
            },

            complete : function(resultat, statut){

            }
        });
    });

    $('.list-group-conversation').on('click', '.list-group-item', function(e){
        e.preventDefault();
        $('.list-group-item').css('backgroundColor', '#fff');
        $('.list-group-item').css('borderRight', 'none');
        $(this).css('backgroundColor', '#eee');
        $(this).css('borderRight', '5px solid #ccc');

        let path = $('#path_show_conversation').attr('data-src');
        let pathCreateMessage = $('#path_create_message').attr('data-src');
        let conversationId = $(this).attr('data-id');
        let token = $('.tokenId').attr("data-token");

        $.ajax({
            url : path,
            type : "POST",
            data: 'conversation=' + conversationId + '&token=' + token,
            success : function(response){
                globalCurrentConversationId = conversationId;
                let myId = response.myId;
                $('#conversation-badge' + conversationId).text('');
                $('.title-messages-section div').remove();
                $('.messages-section div').remove();
                $('.title-messages-section').append('<div>' + response.participant_firstName + ' ' + response.participant_lastName + '</div>');
                $('.create-message-section form').remove();
                $('.create-message-section').append('<form method="post" action="' + pathCreateMessage + '" id="formMessage"><div class="form-group row mx-2"><input id="messageInput" type="text" class="col-9" name="messageInput" placeholder="new message" required/><input id="conversation_id" type="hidden" name="conversationId" value="' + conversationId + '"/><button class="col-2 btn btn-info ml-3" style="max-height:40px;"><i class="fas fa-paper-plane fa-1x"></i></button></div></form>');
                response.messages.forEach(function(message) {
                    let messageSide = response.authors[message.id] == myId ? "message-right" : "message-left";
                    $('.messages-section').append('<div id="message-' + message.id + '" class="' + messageSide + '"><p>' + message.content + '</p><small>' + response.datesMessages[message.id] + '</small></div>');
                });
                $('#message-section').scrollTop($('#message-section')[0].scrollHeight);
            },

            error : function(resultat, statut, erreur){
                
            },

            complete : function(resultat, statut){

            }
        });
    });

    $('.create-message-section').on('submit', '#formMessage', function(e){
        e.preventDefault();
        let url = $('#formMessage').attr('action');
        let dataForm = $(this).serialize();
        let token = $('.tokenId').attr("data-token");

        $.ajax({
            url : url,
            type : "POST",
            data: dataForm + '&token=' + token,
            success : function(response){
                $('#messageInput').val('');
                $('.messages-section').append('<div id="message-' + response.message_id + '" class="message-right"><p>' + response.message_content + '</p><small>' + response.message_createdAt + '</small></div>');
                $('#message-section').scrollTop($('#message-section')[0].scrollHeight);
                let subText = response.lastMessage;
                if (subText.length > 50) {
                    subText = subText.slice(0, 50) + '...';
                }
                $('#list-group-item-' + response.conversationId + ' p').text(subText);
                globalLastId = response.message_id;
            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }
        });
    });

    setInterval(function() {
        let url = "/conversations/refresh";

        $.ajax({
            url : url,
            type : "GET",
            data: 'conversation=' + globalCurrentConversationId + '&lastMessage=' + globalLastId,
            success : function(response){
                let newMessage = false;
                let lastMessageText = '';
                if (!response.empty) {
                    response.messages.forEach(function(message) {
                        let lastMessageDisplayed = $('.messages-section div:last-child').attr('id');
                        if (lastMessageDisplayed != 'message-' + message.id) {
                            let messageSide = response.authors[message.id] == response.myId ? "message-right" : "message-left";
                            $('.messages-section').append('<div id="message-' + message.id + '" class="' + messageSide + '"><p>' + message.content + '</p><small>' + response.datesMessages[message.id] + '</small></div>');
                            globalLastId = message.id;
                            lastMessageText = message.content;
                            newMessage = true;
                        }
                    });
                    if (newMessage) {
                        $('#message-section').scrollTop($('#message-section')[0].scrollHeight);
                        if (lastMessageText.length > 50) {
                            lastMessageText = lastMessageText.slice(0, 50) + '...';
                        }
                        $('#list-group-item-' + globalCurrentConversationId + ' p').text(lastMessageText);
                    }
                }
            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }
        });
    }, 5000);

    setInterval(function() {
        let url = "/conversations/refresh_all";
        let token = $('.tokenId').attr("data-token");

        $.ajax({
            url : url,
            type : "GET",
            data: 'token=' + token,
            success : function(response){
                if (response.length != 0) {
                    if (!response.empty) {
                        response.conversations.forEach(function(conversation) {
                            let lastMessage = response.lastMessage[conversation.id];
                            let userToDisplay = response.usersToDisplay[conversation.id];
                            let globalLastMessageRead = response.lastMessageRead[conversation.id];
                            let lastMessageIdFromConversation = response.lastMessageId[conversation.id];
                            let subtext = '';
                            let badge = '';
                            if (lastMessage == '') {
                                subtext = '<i>no message</i>';
                            } else {
                                if (lastMessage.length > 50) {
                                    lastMessage = lastMessage.slice(0, 50) + '...';
                                }
                                subtext = lastMessage;
                            }
                            if (globalLastMessageRead < lastMessageIdFromConversation) {
                                badge = 'new';
                            }
                            if (globalLastConversationRead < conversation.id) {
                                let elt = $('.list-group-conversation').find('#list-group-item-' + conversation.id);
                                if (elt.length <= 0) {
                                    let elt = '<li id="list-group-item-' + conversation.id + '" class="list-group-item d-flex justify-content-between pr-1 pl-2" data-id="' + conversation.id + '"><div class="w-70"><b class="d-flex w-100 m-0">' + userToDisplay + '</b><p class="d-flex w-100 m-0 text-muted">' + subtext + '</p></div><span id="conversation-badge' + conversation.id + '" class="conversation-badge badge badge-pill">' + badge + '</span></li>';
                                    $('.list-group-conversation').prepend(elt);
                                    globalLastConversationRead = conversation.id;
                                }
                            } else {
                                if (lastMessage == '') {
                                    $('#list-group-item-' + conversation.id + ' p').text('no message').css('font-style', 'italic');
                                } else {
                                    $('#list-group-item-' + conversation.id + ' p').text(subtext);
                                }
                                $('#conversation-badge' + conversation.id).text(badge);
                            }
                        });
                    }
                }
            },

            error : function(resultat, statut, erreur){

            },

            complete : function(resultat, statut){

            }
        });
    }, 10000);
});

const compare = function(ids, asc) {
    return function(row1, row2) {
        const tdValue = function(row, ids) {
        return row.children[ids].textContent;
        }
        const tri = function(v1, v2) {
            if (v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2)) {
                return v1 - v2;
            } else {
                return v1.toString().localeCompare(v2);
            }
            return v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2);
        };
        return tri(tdValue(asc ? row1 : row2, ids), tdValue(asc ? row2 : row1, ids));
    }
}

const tbody = document.querySelector('tbody');
const ths = document.querySelectorAll('th');
const trs = tbody.querySelectorAll('tr');
const icons = document.querySelectorAll('.icon-sort');
icons.forEach(function(icon) {
    icon.addEventListener('click', function() {
        let classe = Array.from(trs).sort(compare(Array.from(ths).indexOf(icon.parentNode), this.asc = !this.asc));
        classe.forEach(function(tr) {
            tbody.appendChild(tr)
        });
    })
});


const input = document.getElementById("myInput");
const table = document.getElementById("myTable");
const tr = table.getElementsByTagName("tr");

input.addEventListener('keyup', function() {
    const filter = input.value.toUpperCase();
    let i = 0;
    let td = null;
    let txtValue = null;
    for (i = 1; i < tr.length; i++) {
        let next = true;
        let countTd = 0;
        let toDisplay = false;
        while(next) {
            td = tr[i].getElementsByTagName("td")[countTd];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    toDisplay = true;
                }
            } else {
                next = false;
            }
            countTd++;
        }
        if (toDisplay) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
    }
});